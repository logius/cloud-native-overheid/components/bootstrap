# Intro

Bootstrap is needed for enterprise clusters with enhanced security features and CI/CD integration. A test cluster does not require the bootstrap tasks. 

# Steps

The bootstrap is performed in multiple steps using Ansible tags, in the order as
depicted below. Some steps are optional.

| Step | AuthN/AuthZ |
| ---- | ---- |
| K8s OIDC client | GitLab environment cicd cluster |
| Admin rolebinding | Beheerder |
| GitLab deployer account | Beheerder |

# Development environment or datacenter cluster

Bootstraps on the laptop are easier than for a bootstrap of a cluster in the
datacenter. The main difference is that in a datacenter, the bootstrap offers
functionality for authenticating a K8s cluster with OIDC ánd managing the
cluster with GitLab pipelines. These steps may be omitted for a development
environment.

To be short, on the development environment, you only need to run `bootstrap -t
monitoring`. See below. Note: you **must** run this step, as it will also
generate the required system namespaces.

## K8s OIDC client
Generate an OIDC client in KeyCloak for the cluster.

This step is required for clusters with OIDC authentication and not needed for
test clusters on the laptop.

The step is executed as follows:
```bash
minikube:/playbook$ ./deploy.sh bootstrap -t k8s_client
```
In order to execute this step, you need Kubectl access to the cluster where
Keycloak is hosted. Access to the cluster that needs to be bootstrapped is not
needed.

The OIDC client secret is reported at the end of this role. Make a copy of the
secret and add this to the Kubectl config file.

## Admin rolebinding
The admin rolebinding is required for clusters with OIDC
authentication. After the admin rolebinding has been set, a user with the
<config.system.admin_role> can log in with admin rights on the cluster.

This step is optional and not needed for test clusters on the laptop.

The step is executed as follows:
```bash
minikube:/playbook$ ./deploy.sh bootstrap -t admin_role
```
In order to execute this step, you need to have Kubectl access to the cluster
that needs to be bootstrapped.

## GitLab deployer account
A service account is needed in order to allow GitLab pipelines to make
deployments to this cluster. This step will create a service account and a K8s
environment in GitLab with the details of this service account. GitLab pipelines
may  use this K8s environment, providing kubectl access to the cluster for the
pipeline code.

This step is optional and not needed for test clusters on the laptop.

The step is executed as follows:
```bash
minikube:/playbook$ ./deploy.sh bootstrap -t gitlab
```
In order to execute this step, you need Kubectl access to the cluster where
GitLab is hosted ánd access to the cluster that needs to be bootstrapped. The
Ansible tasklist operates on two clusters. The cluster that needs to be bootstrapped should be the
default.

## Release CRD

The Release CRD contains information about the CICD pipeline responsible for releasing a new version of a product in a namespace. In platform-toolbox is a generic tasklist that creates this CRD at the end of each run.

The step is executed as follows:
```bash
minikube:/playbook$ ./deploy.sh bootstrap -t release
```

The CRD contains three elements:
- product: Information about the code responsible for the deployment of the product. Typically this refers to an Ansible role.
- cluster: Information about the cluster configuration.
- pipeline: The GitLab pipeline responsible for the deployment.

Example:

```yaml
---
apiVersion: provisioning.standaardplatform.nl/v1alpha1
kind: PlatformProductRelease
metadata:
  name: keycloak-release
info:
  product:
    name: keycloak
    git: gitlab.com/logius/cloud-native-overheid/components/keycloak
    ref: keycloak-v1.0

  cluster:
    name: my-dev
    git: https://gitlab.my-platform.nl/my-platform/deploy/cluster-configs
    ref: master

  pipeline: 
    job: https://gitlab.my-platform.nl/my-platform/deploy/product/keycloak/-/jobs/17136
```

## Openshift OIDC setup

For Openshift clusters an extra step is needed for configuring the OIDC setup.

```bash
minikube:/playbook$ ./deploy.sh bootstrap -t os_oidc -f config1 -f config2 -vv
```

In order to execute this step, you need Kubectl access to the cluster that needs to be bootstrapped.
